package windows;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import game.board.ChessBoard;
import game.board.pieces.*;
import main.MainWindow;

public class GameWindow extends GeneralWindow{
	ChessBoard _chess_board;
	final int black = 1;
	final int white = 0;
	public GameWindow(MainWindow main_window) {
		super(main_window,false);
		//add(new JLabel("game"));
		setVisible(true);
		
		setMinimumSize(new Dimension(100,200));
		setMaximumSize(new Dimension(100,500));
		setLayout(new GridLayout(8,8));
		
		_chess_board = new ChessBoard(this);
		_chess_board.place(0, 0, new Rook());
		_chess_board.place(1, 0, new Knight());
		_chess_board.place(2, 0, new Bishop());
		_chess_board.place(3, 0, new Queen());
		_chess_board.place(4, 0, new King());
		_chess_board.place(5, 0, new Bishop());
		_chess_board.place(6, 0, new Knight());
		_chess_board.place(7, 0, new Rook());

		_chess_board.place(0, 7, new Rook());
		_chess_board.place(1, 7, new Knight());
		_chess_board.place(2, 7, new Bishop());
		_chess_board.place(3, 7, new Queen());
		_chess_board.place(4, 7, new King());
		_chess_board.place(5, 7, new Bishop());
		_chess_board.place(6, 7, new Knight());
		_chess_board.place(7, 7, new Rook());
		
		for(int i = 0; i < 8; i++) {
			_chess_board.place(i, 6, new Pawn());
			_chess_board.place(i, 1, new Pawn());
		}
		
	}
}
