package game.board;

import javax.swing.JButton;

public class ChessTile extends JButton{
	ChessPiece _current_piece;
	int _x;
	int _y;
	public ChessTile(int x, int y, ChessPiece current_piece) {
		_x = x;
		_y = y;
		//setText("("+x+","+y+")");
		_current_piece = current_piece;
		/*
		setBorderPainted(false);
		setFocusPainted(false);
		setContentAreaFilled(false);
		*/
	}
	public void addPiece(ChessPiece piece) {
		_current_piece = piece;
		setText(_current_piece.getText());
	}
}
