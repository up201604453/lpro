package game.board;

import java.util.ArrayList;

import javax.swing.JPanel;

import game.board.pieces.Rook;

public class ChessBoard{
	ArrayList<ArrayList<ChessTile>> _inner_array;
	public ChessBoard(JPanel game_panel) {
		_inner_array = new ArrayList<ArrayList<ChessTile>>(); 
		for(int i = 0; i < 8; i++) {
			_inner_array.add(new ArrayList<ChessTile>());
			for(int j = 0; j < 8; j++) {
				_inner_array.get(i).add(j, new ChessTile(j,i,null));
				game_panel.add(_inner_array.get(i).get(j));
			}
		}
	}
	public void place(int x, int y, ChessPiece piece) {
		_inner_array.get(x).get(y).addPiece(piece);;
	}
	/*
	public void setAt(int x, int y, ChessSlot obj) {
		if(x > 7 || y > 7 || x < 0 || y < 0) {
			System.out.println("Cannot setAt x: "+x+" y:"+y);
		}else {
			_inner_array.get(x).remove(y);
			_inner_array.get(x).add(y, obj);
		}
	}
	public ChessSlot getAt(int x, int y) {
		if(x > 7 || y > 7 || x < 0 || y < 0) {
			System.out.println("Cannot getAt x: "+x+" y:"+y);
			return null;
		}else {
			return _inner_array.get(x).get(y);
		}
	}
	*/
	public void moveTo(int from_x, int from_y, int to_x, int to_y, ChessPiece piece) {
		// TODO Auto-generated method stub
		
	}
}
