package game.board.pieces;

import game.board.ChessPiece;

public class Queen extends ChessPiece{
	public Queen() {
		setText("Queen");
	}
}
