package game.board.pieces;

import game.board.ChessPiece;

public class Bishop extends ChessPiece{
	public Bishop() {
		setText("Bishop");
	}
}
