package game.board.pieces;

import game.board.ChessPiece;

public class Pawn extends ChessPiece{
	public Pawn() {
		setText("Pawn");
	}
}
