package game.board.pieces;

import game.board.ChessPiece;

public class Knight extends ChessPiece{
	public Knight() {
		setText("Knight");
	}
}
