package game.board.pieces;

import game.board.ChessPiece;

public class King extends ChessPiece{
	public King() {
		setText("King");
	}
}
