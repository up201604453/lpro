package game.board.pieces;

import game.board.ChessPiece;

public class Rook extends ChessPiece{
	public Rook() {
		setText("Rook");
	}
}
