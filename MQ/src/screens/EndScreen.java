package screens;

import javax.swing.*;

public class EndScreen extends JFrame {
    private static final long serialVersionUID = -400351073254486718L;
    Canvas img;
    public EndScreen (String screen){
    	img = new Canvas(screen);
    	getContentPane().add(img);
    	//getContentPane().add(img, BorderLayout.PAGE_START);
        setSize(500,500);
        setVisible(true);
    } 
}
